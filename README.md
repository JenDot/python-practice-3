# Python Practice 3: Beginner Projects from Kylie Ying at FreeCodeCamp

**This repo is a series of 12 exercises:**
1. Madlibs 
2. Guess the Number (computer) 
3. Guess the Number (user)
4. Rock Paper Scissors
5. Hangman
6. Tic-Tac-Toe
7. Tic-Tac-Toe AI
8. Binary Search 
9. Minesweeper 
10. Sudoku Solver 
11. Photo Manipulation in Python 
12. Markov Chain Text Composer

[Kylie Ying's Github Repo] (https://github.com/kying18/beginner-projects)
[Youtube Link] (https://www.youtube.com/watch?v=8ext9G7xspg)