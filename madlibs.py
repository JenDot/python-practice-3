"""Simple Mad Libs program to practice concatenation."""

#  Write a function to run a simple Mad Libs game

# function(no parameters) 
# variables to handle user input for word blanks
    # variables should indicate type of input (noun, verb, etc)
# madlib = multi-line f-string with variable calls
# call function

def madlib():
    body_part = input("Body part >> ")
    verb = input("Verb >> ")
    adj1 = input("Adjective >> ")
    adj2 = input("Another adjective >> ")
    adj3 = input("Another adjective >> ")
    adj4 = input("Another adjective >> ")
    adj5 = input("Another adjective >> ")
    noun1 = input("Noun >> ")
    noun2 = input("Another noun >> ")
    plural_noun1 = input("Plural noun >> ")
    plural_noun2 = input("Another plural noun >> ")

    madlib = f"I love computer programming because it's {adj1}! The journey to becoming a \
good programmer starts with hope in your mind and a dream in your {body_part}. Through blood, \
sweat, and {plural_noun1}, hopefully it never ends. Yes, once you learn to {verb}, it becomes \
a part of your life identity and you can become a super {adj2} hacker. Knowledge of programming \
lets you take control of your {noun1}. You can create your own personal {plural_noun2}, anything \
from developing {adj3} software to analyzing data and making predictions about the {noun2}. You can \
maybe even recreate Jarvis and make him extra {adj4}. I hope you'll start your {adj5} journey soon!"

    print(madlib)

madlib()

